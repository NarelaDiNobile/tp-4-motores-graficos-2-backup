using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Piso_GameOver : MonoBehaviour
{
    public GameObject gameOverCanvas; // Referencia al objeto Canvas de Game Over

    private bool hasCollided = false; // Variable para controlar si ha ocurrido una colisi�n

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso") && !hasCollided) // Verificar la etiqueta del piso y si no ha ocurrido una colisi�n anteriormente
        {
            hasCollided = true; // Marcar que ha ocurrido una colisi�n

            // Mostrar el Canvas de Game Over
            gameOverCanvas.SetActive(true);
            Time.timeScale = 1f;
        }
    }

 


    public void Retry()
    {
        SceneManager.LoadScene("Juego3");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
