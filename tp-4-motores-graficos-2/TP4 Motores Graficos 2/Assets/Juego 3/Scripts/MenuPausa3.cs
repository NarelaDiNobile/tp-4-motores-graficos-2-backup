using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa3 : MonoBehaviour
{
    public GameObject pausemenu3;
    public string sceneName3;
    public bool toggle3;
    public Controller_Player playerScript3;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            toggle3 = !toggle3;
            if (toggle3 == false)
            {
                pausemenu3.SetActive(false);
                AudioListener.pause = false;
                Time.timeScale = 1;
                playerScript3.enabled = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (toggle3 == true)
            {
                pausemenu3.SetActive(true);
                AudioListener.pause = true;
                Time.timeScale = 0;
                playerScript3.enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
    public void resumeGame()
    {
        toggle3 = false;
        pausemenu3.SetActive(false);
        AudioListener.pause = false;
        Time.timeScale = 1;
        playerScript3.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void quitToMenu()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        SceneManager.LoadScene(sceneName3);
    }
    public void quitToDesktop()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        Application.Quit();
    }
}
