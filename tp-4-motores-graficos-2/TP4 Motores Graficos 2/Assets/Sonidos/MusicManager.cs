using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    public AudioClip defaultMusic; // M�sica predeterminada para las primeras dos escenas
    public AudioClip[] sceneMusic; // Arreglo de m�sicas para las dem�s escenas

    private AudioSource audioSource;

    private void Awake()
    {
        // Busca si ya hay un objeto MusicManager en la escena actual
        MusicManager[] musicManagers = FindObjectsOfType<MusicManager>();
        if (musicManagers.Length > 1)
        {
            // Si hay m�s de un objeto MusicManager, destruye este para evitar duplicados
            Destroy(gameObject);
        }
        else
        {
            // Si este es el �nico objeto MusicManager, no lo destruyas al cargar una nueva escena
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        PlaySceneMusic();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        StopPreviousMusic();
        PlaySceneMusic();
    }

    private void StopPreviousMusic()
    {
        // Det�n la m�sica anterior si se est� reproduciendo
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }

    private void PlaySceneMusic()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;

        // Verifica el �ndice de la escena actual y elige la m�sica correspondiente
        AudioClip musicToPlay;
        if (sceneIndex < 2)
        {
            musicToPlay = defaultMusic;
        }
        else
        {
            int musicIndex = sceneIndex - 2;
            if (musicIndex < sceneMusic.Length)
            {
                musicToPlay = sceneMusic[musicIndex];
            }
            else
            {
                musicToPlay = defaultMusic;
            }
        }

        // Reproduce la m�sica seleccionada
        audioSource.clip = musicToPlay;
        audioSource.Play();
    }
}
