using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int Score;
    public bool IsGameOver;

    [SerializeField] private TMP_Text scoreText;

    [SerializeField] private GameObject gameOverMenu;

    [SerializeField] private TMP_Text gameoverScoreText;
    [SerializeField] private TMP_Text gameoverRecordText;
    internal static int actualPlayer;


    public void AddScore ()
    {
        Score++;
        scoreText.text = Score.ToString();
    }

    public void SetGameOver ()
    {
        IsGameOver = true;

        gameOverMenu.SetActive(true);

        int record = PlayerPrefs.GetInt("Record", 0);

        if (Score > record) 
        {
            record = Score;
            PlayerPrefs.SetInt("Record", record);
        }

        gameoverScoreText.text = "Score : " + Score.ToString();
        gameoverRecordText.text = "Record : " + record.ToString();

    }

    public void Retry ()
    {
        SceneManager.LoadScene("Juego1");
    }

    public void Exit ()
    {
        Application.Quit();
    }

}
