using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuboSpawner : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;

    [SerializeField] private Tubo TuboPrefab;

    [SerializeField] private float spawnMaxY;
    [SerializeField] private float spawnMinY;

    [SerializeField] private float spawnTime;


    float timer = 0;

    private void Update()
    {
        if (gameManager.IsGameOver) return;

        timer += Time.deltaTime;

        if (timer >= spawnTime)
        {
            timer = 0f;
            SpawnTubo();
        }
    }

    private void SpawnTubo ()
    {
        Vector3 spawnPos = new Vector3(transform.position.x, Random.Range(spawnMinY, spawnMaxY), transform.position.z);

        Instantiate(TuboPrefab, spawnPos, Quaternion.identity);
    }
}
