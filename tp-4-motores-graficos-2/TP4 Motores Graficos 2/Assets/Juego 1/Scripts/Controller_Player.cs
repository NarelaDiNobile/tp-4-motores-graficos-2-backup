using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;

    [SerializeField] private Rigidbody PlayerRigidbody;

    [SerializeField] private float jumpForce;

    [SerializeField] private AudioSource audioSource;

    [SerializeField] private AudioClip addSound;
    [SerializeField] private AudioClip crashSound;
    private void Update()
    {
        if (gameManager.IsGameOver) return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlayerRigidbody.velocity = Vector3.up * jumpForce;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag ("Tubo"))
        {
            gameManager.AddScore();
            audioSource.PlayOneShot(addSound);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        gameManager.SetGameOver();
        audioSource.PlayOneShot(crashSound);
    }

}
