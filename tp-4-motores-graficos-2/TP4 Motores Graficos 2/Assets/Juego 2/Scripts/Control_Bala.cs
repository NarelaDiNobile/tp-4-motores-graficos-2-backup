using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Control_Bala : MonoBehaviour
{
    private float balaVelocity;
    private Rigidbody rb;
    public static bool GameOverScreen;
    
    // Start is called before the first frame update
    void Start()
    {
        GameOverScreen = false;
        rb = GetComponent<Rigidbody>();
        balaVelocity = 10;
        


    }

    // Update is called once per frame
    void Update()
    {
        
        
        rb.AddForce(new Vector3(balaVelocity, 0, 0), ForceMode.Force);
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Pelota"))
        {
            rb.GetComponent<Rigidbody>().velocity = Vector3.zero;
            balaVelocity = 0;
            rb.transform.parent = collision.transform;
            rb.constraints |= RigidbodyConstraints.FreezePosition;
         
        }
        else if (collision.gameObject.CompareTag("Proyectil"))
        {
            GameOverScreen = true;
        }
    }
}
