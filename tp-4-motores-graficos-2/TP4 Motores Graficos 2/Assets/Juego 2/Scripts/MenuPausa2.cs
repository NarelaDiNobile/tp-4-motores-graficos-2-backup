using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa2 : MonoBehaviour
{
    public GameObject pausemenu2;
    public string sceneName2;
    public bool toggle2;
    public Controller_Player playerScript2;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            toggle2 = !toggle2;
            if (toggle2 == false)
            {
                pausemenu2.SetActive(false);
                AudioListener.pause = false;
                Time.timeScale = 1;
                playerScript2.enabled = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (toggle2 == true)
            {
                pausemenu2.SetActive(true);
                AudioListener.pause = true;
                Time.timeScale = 0;
                playerScript2.enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
    public void resumeGame()
    {
        toggle2 = false;
        pausemenu2.SetActive(false);
        AudioListener.pause = false;
        Time.timeScale = 1;
        playerScript2.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void quitToMenu()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        SceneManager.LoadScene(sceneName2);
    }
    public void quitToDesktop()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        Application.Quit();
    }
}
