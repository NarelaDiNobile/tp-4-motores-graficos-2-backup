using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa4 : MonoBehaviour
{
    public GameObject pausemenu4;
    public string sceneName4;
    public bool toggle4;
    public Controller_Player playerScript4;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            toggle4 = !toggle4;
            if (toggle4 == false)
            {
                pausemenu4.SetActive(false);
                AudioListener.pause = false;
                Time.timeScale = 1;
                playerScript4.enabled = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (toggle4 == true)
            {
                pausemenu4.SetActive(true);
                AudioListener.pause = true;
                Time.timeScale = 0;
                playerScript4.enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
    public void resumeGame()
    {
        toggle4 = false;
        pausemenu4.SetActive(false);
        AudioListener.pause = false;
        Time.timeScale = 1;
        playerScript4.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void quitToMenu()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        SceneManager.LoadScene(sceneName4);
    }
    public void quitToDesktop()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        Application.Quit();
    }
}
