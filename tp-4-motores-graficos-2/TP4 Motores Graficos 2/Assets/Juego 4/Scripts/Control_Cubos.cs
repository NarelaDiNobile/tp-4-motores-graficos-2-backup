using UnityEngine;

public class Control_Cubos : MonoBehaviour
{
    private Rigidbody rb; // Referencia al componente Rigidbody
    private bool isFloating; // Variable para controlar si el objeto est� flotando
    private Renderer cubeRenderer; // Referencia al componente Renderer
    private Color originalColor; // Color original del cubo

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); // Obtener referencia al componente Rigidbody
        if (rb == null)
        {
            rb = gameObject.AddComponent<Rigidbody>(); // Agregar componente Rigidbody si no existe
        }
        rb.useGravity = false; // Desactivar la gravedad al inicio

        // Establecer la velocidad inicial para hacer que los cubos floten
        rb.velocity = Vector3.up * 2f;

        isFloating = true; // Establecer el estado de flotaci�n inicial

        // Obtener referencia al componente Renderer y guardar el color original del cubo
        cubeRenderer = GetComponent<Renderer>();
        originalColor = cubeRenderer.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        // Detectar si se hizo clic con el mouse sobre el cubo
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                // Verificar si el objeto golpeado es este cubo
                if (hit.collider.gameObject == gameObject)
                {
                    if (isFloating)
                    {
                        // Cubo flotando: aplicar la gravedad correspondiente
                        rb.useGravity = true;
                        rb.velocity = Vector3.down * 2f;
                    }
                    else
                    {
                        // Cubo no flotando: volver a subir
                        rb.useGravity = false;
                        rb.velocity = Vector3.up * 2f;
                    }
                    isFloating = !isFloating; // Cambiar el estado de flotaci�n del objeto

                    // Cambiar el color del cubo
                    ChangeCubeColor();
                }
            }
        }
    }

    private void ChangeCubeColor()
    {
        // Verificar si el color actual es el color original del cubo
        if (cubeRenderer.material.color == originalColor)
        {
            // Cambiar el color a otro color (por ejemplo, rojo)
            cubeRenderer.material.color = Color.red;
        }
        else
        {
            // Cambiar el color de vuelta al color original
            cubeRenderer.material.color = originalColor;
        }
    }
}
